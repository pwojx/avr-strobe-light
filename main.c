// Copyright (c) 2022 Paweł Wojaczek

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>

#include <stdio.h>

#include "utils.h"
#include "encoder.h"
#include "lcd_44780.h"
#include "custom_chars.h"

// Minimum battery level, below which the device should be powered off.
#define MIN_BAT_VOLTAGE_mV 9000

// Maximum value of the `freq` variable. The actual frequency of the strobe light
// is calculated according to the following formula: f = 10000 / (freq + 1) [Hz].
#define MAX_FREQ           9999

// The time used to debounce the encoder switch (fine/coarse adjustment).
#define DEBOUNCE_TIME_MS   50

// The pin used to control the LCD backlight.
#define LCD_BACKLIGHT_REG  C
#define LCD_BACKLIGHT_POS  3

// The pin to which the enable toggle switch is connected.
#define ENABLE_REG         B
#define ENABLE_POS         1

// The pin used to control the strobe light LED.
#define STROBE_REG         B
#define STROBE_POS         2

// The pin to which the encoder switch is connected.
#define ENC_SWITCH_REG     D
#define ENC_SWITCH_POS     4

// The pin to which the frequency/duty cycle toggle switch is connected.
#define FREQ_DUTY_SW_REG   B
#define FREQ_DUTY_SW_POS   0

// The ADC channel used to measure the battery voltage, connected through a voltage divider.
#define BAT_ADC_POS        4

static uint16_t read_adc_data(void);
static void goto_sleep(void);
static void low_battery_level(uint16_t bat_voltage);
static inline void INTn_ISR(void);

static char buf[17];
volatile uint8_t enc_step = 1;
volatile uint16_t freq = 9, duty = 1;

int main(void) {
    // turn on the LCD backlight
    DDR(LCD_BACKLIGHT_REG) |= (1 << LCD_BACKLIGHT_POS);
    PORT(LCD_BACKLIGHT_REG) |= (1 << LCD_BACKLIGHT_POS);

    // enable internal pull-up resitors for switches
    PORT(ENABLE_REG) |= (1 << ENABLE_POS);
    PORT(ENC_SWITCH_REG) |= (1 << ENC_SWITCH_POS);
    PORT(FREQ_DUTY_SW_REG) |= (1 << FREQ_DUTY_SW_POS);

    // turn off the strobe light LED
    DDR(STROBE_REG) |= (1 << STROBE_POS);

    // initialize the encoder
    encoder_init();

    // initialize external interrupts
    EICRA |= (1 << ISC10) | (1 << ISC00);
    EIMSK |= (1 << INT1) | (1 << INT0);

    // initialize the LCD
    lcd_init();
    lcd_define_char(CHAR_BOX_EMPTY, box_empty);
    lcd_define_char(CHAR_BOX_FULL, box_full);
    lcd_define_char(CHAR_FINE_ADJ, fine_adjustment);
    lcd_define_char(CHAR_COARSE_ADJ, coarse_adjustment);

    // intitialize the Timer/Counter1
    TCCR1B |= (1 << WGM12);  // CTC mode
    TCCR1B |= (1 << CS10);   // no prescaling
    OCR1A = 1599; // OCR1AH = 1599 >> 8;
                  // OCR1AL = 1599 & 0xff;
    TIMSK1 |= (1 << OCIE1A); // enable Output Compare A Match Interrupt

    // disable the Analog Comparator
    ACSR |= (1 << ACD);

    // initialize the ADC
    ADMUX |= (1 << REFS1) | (1 << REFS0);  // Vref = 2.56 V (internal reference)
    ADMUX |= BAT_ADC_POS;                  // select input channel
    DIDR0 |= (1 << BAT_ADC_POS);           // disable digital input
    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // ADC prescaler = 128
//  ADCSRA |= (1 << ADATE);                // enable auto trigger
    ADCSRA |= (1 << ADEN);                 // enable ADC
    ADCSRA |= (1 << ADSC);                 // start conversion

    // display the startup screen
    lcd_home();
    lcd_print_str("AVR Strobe Light");
    lcd_set_pos(0, 1);
    lcd_print_str(" ** pw, 2022 ** ");
    _delay_ms(3000);

    // check the battery voltage
    // V = 10-bit resolution *     Vref    * voltage divider scale factor
    // V =    (adc / 1023)   * (256 / 100) *        ((10 + 2) / 2)
    uint16_t bat_voltage_mV = (read_adc_data() * 15360UL) / 1023;
    if (bat_voltage_mV < MIN_BAT_VOLTAGE_mV) {
        low_battery_level(bat_voltage_mV);
        goto_sleep();
    }

    // clear the display
    lcd_clear();

    // initialize a loop counter variable
    uint8_t loop_cnt = 0;

    // enable interrupts globally
    sei();

    while (1) {
        // read and debounce the encoder switch, toggle fine/coarse adjustment
        if (!(PIN(ENC_SWITCH_REG) & (1 << ENC_SWITCH_POS))) {
            _delay_ms(DEBOUNCE_TIME_MS);
            if (!(PIN(ENC_SWITCH_REG) & (1 << ENC_SWITCH_POS))) {
                enc_step = (enc_step == 1) ? 10 : 1;
                _delay_ms(4*DEBOUNCE_TIME_MS);
            }
        }

        // return home
        lcd_home();
        _delay_ms(20);

        // read the frequency/duty cycle toggle switch and draw an arrow on the display
        uint8_t freq_duty_sw = PIN(FREQ_DUTY_SW_REG) & (1 << FREQ_DUTY_SW_POS);
        lcd_putc(freq_duty_sw ? '~' : ' ');

        // calculate the RPM based on the `freq` variable
        lcd_set_pos(2, 0);
        uint32_t crpm = 60000000UL / (freq + 1); // RPM = 100 cRPM (centiRPM)
        uint32_t rpm_1 = crpm / 100; // integral part
        uint8_t  rpm_2 = crpm % 100; // fractional part

        // display the RPM on the LCD
        if (freq == 0) {
            lcd_print_str(" FLASHLIGHT ");
        } else if (crpm >= 1000000UL) {
            sprintf(buf, "RPM: %7lu", rpm_1);
            lcd_print_str(buf);
        } else {
            sprintf(buf, "RPM: %4lu.%02u", rpm_1, rpm_2);
            lcd_print_str(buf);
        }

        // draw a symbol on the display to indicate fine/coarse adjustment
        lcd_set_pos(15, 0);
        lcd_putc((enc_step == 1) ? CHAR_FINE_ADJ : CHAR_COARSE_ADJ);

        // draw an arrow on the display
        lcd_set_pos(0, 1);
        lcd_putc(freq_duty_sw ? ' ' : '~');

        // calculate the duty cycle
        lcd_set_pos(2, 1);
        uint16_t duty_cycle = 10000UL * duty / (freq + 1);
        uint8_t duty_1 = duty_cycle / 100; // integral part
        uint8_t duty_2 = duty_cycle % 100; // fractional part

        // measure the battery voltage every 32nd iteration
        if (!(loop_cnt & 0x1f)) {
            // 10-bit resolution *     Vref    * voltage divider scale factor
            //    (adc / 1023)   * (256 / 100) *        ((10 + 2) / 2)
            bat_voltage_mV = (read_adc_data() * 15360UL) / 1023;
//            if (bat_voltage_mV < 0.9 * MIN_BAT_VOLTAGE_mV) {
//                low_battery_level(bat_voltage_mV);
//            }
        }
        uint8_t bat_1 = bat_voltage_mV / 1000;       // integral part
        uint8_t bat_2 = (bat_voltage_mV / 10) % 100; // fractional part

        // display the duty cycle or the battery voltage on the LCD
        if (duty_cycle == 10000UL) {
            sprintf(buf, "   %2u.%02uV   ", bat_1, bat_2);
            lcd_print_str(buf);
        } else {
            sprintf(buf, "DUTY: %2u.%02u%%", duty_1, duty_2);
            lcd_print_str(buf);
        }

        // draw a symbol on the display to indicate if the LED is on
        lcd_set_pos(15, 1);
        lcd_putc(PIN(ENABLE_REG) & (1 << ENABLE_POS) ? CHAR_BOX_EMPTY : CHAR_BOX_FULL);

        loop_cnt++;
    }
}

/*
 * Returns the result of an ADC conversion.
 */
static uint16_t read_adc_data(void) {
    uint16_t res;

    ADCSRA |= (1 << ADSC);          // start the conversion
    while (ADCSRA & (1 << ADSC)) {} // wait until the conversion is complete

    res = ADCL;                     // read the result of the conversion
    res |= (ADCH << 8);

    return res;
}

/*
 * Puts the MCU into Power-down sleep mode.
 *
 * Interrupts are globally disabled before entering the sleep mode to prevent
 * the MCU from waking up.
 */
static void goto_sleep(void) {
    cli();

    // turn off the strobe light and the lcd backlight
    PORT(STROBE_REG) &= ~(1 << STROBE_POS);
    PORT(LCD_BACKLIGHT_REG) &= ~(1 << LCD_BACKLIGHT_POS);

    // disable ADC
    ADCSRA &= ~(1 << ADEN);

    // shut down peripherals
    PRR0 = 0xff;

    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();
    sleep_bod_disable();
    sleep_cpu();
    sleep_disable();
}

/*
 * Displays a low battery message on the LCD, shows the current battery voltage
 * and flashes the LCD backlight several times.
 */
static void low_battery_level(uint16_t bat_voltage) {
    uint8_t bat_1 = bat_voltage / 1000;
    uint8_t bat_2 = (bat_voltage / 10) % 100;

    lcd_home();
    lcd_print_str("LOW BAT. VOLTAGE");
    lcd_set_pos(0, 1);
    sprintf(buf, " --\x7e %2u.%02uV \x7f-- ", bat_1, bat_2);
    lcd_print_str(buf);

    _delay_ms(3000);
    for (uint8_t i = 0; i < 10; ++i) {
        PORT(LCD_BACKLIGHT_REG) ^= (1 << LCD_BACKLIGHT_POS);
        _delay_ms(750);
    }
}

/*
 * Processes the output of the encoder and sets the frequency or the duty cycle.
 */
static inline void INTn_ISR(void) {
    uint8_t result = encoder_process();

    // set the frequency
    if (PIN(FREQ_DUTY_SW_REG) & (1 << FREQ_DUTY_SW_POS)) {
        if (result == ENC_DIR_CCW && freq <= MAX_FREQ - enc_step) {
            freq += enc_step;
        } else if (result == ENC_DIR_CW && freq >= enc_step) {
            freq -= enc_step;

            // make sure that the duty cycle is not greater than the frequency
            if (freq == 0) duty = 1;
            else if (duty > freq) duty = freq;
        }
    }

    // set the duty cycle
    else {
        if (result == ENC_DIR_CCW && (int16_t)duty <= (int16_t)(freq - enc_step)) {
            duty += enc_step;
        } else if (result == ENC_DIR_CW && duty >= enc_step + 1) {
            duty -= enc_step;
        }
    }
}

/*
 * External Interrupt Request 0
 * INT0: ENC_A
 */
ISR(INT0_vect) {
    INTn_ISR();
}

/*
 * External Interrupt Request 1
 * INT1: ENC_B
 */
ISR(INT1_vect) {
    INTn_ISR();
}

/*
 * Timer/Counter1 Compare Match A
 * T = 100 us
 */
ISR(TIMER1_COMPA_vect) {
    static uint32_t t;

    if (PIN(ENABLE_REG) & (1 << ENABLE_POS))
        PORT(STROBE_REG) &= ~(1 << STROBE_POS);
    else if (t < duty)
        PORT(STROBE_REG) |= (1 << STROBE_POS);
    else if (t == duty)
        PORT(STROBE_REG) &= ~(1 << STROBE_POS);

    if (++t > freq) t = 0;
}
