#ifndef CUSTOM_CHARS_H_
#define CUSTOM_CHARS_H_

#define CHAR_BOX_EMPTY   0
#define CHAR_BOX_FULL    1
#define CHAR_FINE_ADJ    2
#define CHAR_COARSE_ADJ  3

const uint8_t box_empty[8] = {
    0b00000,
    0b11111,
    0b10001,
    0b10001,
    0b10001,
    0b11111,
    0b00000,
    0b00000
};

const uint8_t box_full[8] = {
    0b00000,
    0b11111,
    0b11111,
    0b11111,
    0b11111,
    0b11111,
    0b00000,
    0b00000
};

const uint8_t fine_adjustment[8] = {
    0b00000,
    0b00000,
    0b00100,
    0b01010,
    0b00100,
    0b00000,
    0b00000,
    0b00000
};

const uint8_t coarse_adjustment[8] = {
    0b00000,
    0b01110,
    0b10001,
    0b10001,
    0b10001,
    0b01110,
    0b00000,
    0b00000
};

#endif /* CUSTOM_CHARS_H_ */
