#ifndef LCD_44780_H_
#define LCD_44780_H_

#include <stdint.h>

/*************** LCD config ***************/
#define LCD_USE_8BIT_MODE   0
#define LCD_USE_BUSY_FLAG   1

#define LCD_COLS            16
#define LCD_ROWS            2

#define LCD_RS_REG          C
#define LCD_RS_POS          0

#if LCD_USE_BUSY_FLAG == 1
    #define LCD_RW_REG      C
    #define LCD_RW_POS      1
#endif

#define LCD_EN_REG          C
#define LCD_EN_POS          2

#if LCD_USE_8BIT_MODE == 1
    #define LCD_D0_REG      X
    #define LCD_D0_POS      0
    #define LCD_D1_REG      X
    #define LCD_D1_POS      1
    #define LCD_D2_REG      X
    #define LCD_D2_POS      2
    #define LCD_D3_REG      X
    #define LCD_D3_POS      3
#endif

#define LCD_D4_REG          C
#define LCD_D4_POS          4
#define LCD_D5_REG          C
#define LCD_D5_POS          5
#define LCD_D6_REG          C
#define LCD_D6_POS          6
#define LCD_D7_REG          C
#define LCD_D7_POS          7
/******************************************/

/***         LCD line addresses         ***/
#define LCD_LINE0       0x00
#define LCD_LINE1       0x40
#define LCD_LINE2       0x14
#define LCD_LINE3       0x54
/******************************************/

/***          LCD instructions          ***/
#define LCD_CLEAR_DISPLAY               0x01
#define LCD_RETURN_HOME                 0x02
#define LCD_ENTRY_MODE_SET              0x04
#define LCD_DISPLAY_ON_OFF_CONTROL      0x08
#define LCD_CURSOR_OR_DISPLAY_SHIFT     0x10
#define LCD_FUNCTION_SET                0x20
#define LCD_SET_CGRAM_ADDRESS           0x40
#define LCD_SET_DDRAM_ADDRESS           0x80
/******************************************/

/***            LCD options             ***/
#define LCD_INCREMENT                   0x02
#define LCD_DECREMENT                   0x00

#define LCD_ACCOMPANIES_DISPLAY_SHIFT   0x01

#define LCD_DISPLAY_ON                  0x04
#define LCD_DISPLAY_OFF                 0x00

#define LCD_CURSOR_ON                   0x02
#define LCD_CURSOR_OFF                  0x00

#define LCD_BLINK_ON                    0x01
#define LCD_BLINK_OFF                   0x00

#define LCD_DISPLAY_SHIFT               0x08
#define LCD_CURSOR_MOVE                 0x00

#define LCD_SHIFT_TO_THE_RIGHT          0x08
#define LCD_SHIFT_TO_THE_LEFT           0x00

#define LCD_8_BITS                      0x10
#define LCD_4_BITS                      0x00

#define LCD_2_LINES                     0x08
#define LCD_1_LINE                      0x00

#define LCD_5x10_DOTS                   0x04
#define LCD_5x8_DOTS                    0x00
/******************************************/

/***           LCD functions            ***/
void lcd_init(void);

void lcd_clear(void);
void lcd_home(void);
void lcd_set_pos(uint8_t x, uint8_t y);

void lcd_putc(char c);
void lcd_print_str(char *s);
void lcd_print_dec(int32_t val);
void lcd_print_hex(uint32_t val);

void lcd_define_char(uint8_t idx, const uint8_t *char_map);
/******************************************/

#endif /* LCD_44780_H_ */
