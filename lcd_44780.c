// Copyright (c) 2022 Paweł Wojaczek

#include "lcd_44780.h"

#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include "utils.h"

/*
 * Sets D0-D7 (or D4-D7) pins as inputs.
 */
static inline void _lcd_d_input(void) {
#if LCD_USE_8BIT_MODE == 1
    DDR(LCD_D0_REG) &= ~(1 << LCD_D0_POS);
    DDR(LCD_D1_REG) &= ~(1 << LCD_D1_POS);
    DDR(LCD_D2_REG) &= ~(1 << LCD_D2_POS);
    DDR(LCD_D3_REG) &= ~(1 << LCD_D3_POS);
#endif

    DDR(LCD_D4_REG) &= ~(1 << LCD_D4_POS);
    DDR(LCD_D5_REG) &= ~(1 << LCD_D5_POS);
    DDR(LCD_D6_REG) &= ~(1 << LCD_D6_POS);
    DDR(LCD_D7_REG) &= ~(1 << LCD_D7_POS);
}

/*
 * Sets D0-D7 (or D4-D7) pins as outputs.
 */
static inline void _lcd_d_output(void) {
#if LCD_USE_8BIT_MODE == 1
    DDR(LCD_D0_REG) |= (1 << LCD_D0_POS);
    DDR(LCD_D1_REG) |= (1 << LCD_D1_POS);
    DDR(LCD_D2_REG) |= (1 << LCD_D2_POS);
    DDR(LCD_D3_REG) |= (1 << LCD_D3_POS);
#endif

    DDR(LCD_D4_REG) |= (1 << LCD_D4_POS);
    DDR(LCD_D5_REG) |= (1 << LCD_D5_POS);
    DDR(LCD_D6_REG) |= (1 << LCD_D6_POS);
    DDR(LCD_D7_REG) |= (1 << LCD_D7_POS);
}

/*
 * Gets the values of D0-D7 (or D4-D7) pins and returns them as a byte.
 */
#if LCD_USE_BUSY_FLAG == 1
static inline uint8_t _lcd_get_data_pins(void) {
    uint8_t res = 0;
#if LCD_USE_8BIT_MODE == 1
    if (PIN(LCD_D0_REG) & (1 << LCD_D0_POS))
        res |= (1 << 0);

    if (PIN(LCD_D1_REG) & (1 << LCD_D1_POS))
        res |= (1 << 1);

    if (PIN(LCD_D2_REG) & (1 << LCD_D2_POS))
        res |= (1 << 2);

    if (PIN(LCD_D3_REG) & (1 << LCD_D3_POS))
        res |= (1 << 3);

    if (PIN(LCD_D4_REG) & (1 << LCD_D4_POS))
        res |= (1 << 4);

    if (PIN(LCD_D5_REG) & (1 << LCD_D5_POS))
        res |= (1 << 5);

    if (PIN(LCD_D6_REG) & (1 << LCD_D6_POS))
        res |= (1 << 6);

    if (PIN(LCD_D7_REG) & (1 << LCD_D7_POS))
        res |= (1 << 7);
#else
    if (PIN(LCD_D4_REG) & (1 << LCD_D4_POS))
        res |= (1 << 0);

    if (PIN(LCD_D5_REG) & (1 << LCD_D5_POS))
        res |= (1 << 1);

    if (PIN(LCD_D6_REG) & (1 << LCD_D6_POS))
        res |= (1 << 2);

    if (PIN(LCD_D7_REG) & (1 << LCD_D7_POS))
        res |= (1 << 3);
#endif
    return res;
}
#endif

/*
 * Sets D0-D7 (or D4-D7) pins according to `data` bits.
 */
static inline void _lcd_set_data_pins(uint8_t data) {
#if LCD_USE_8BIT_MODE == 1
    if (data & (1 << 0))
        PORT(LCD_D0_REG) |= (1 << LCD_D0_POS);
    else
        PORT(LCD_D0_REG) &= ~(1 << LCD_D0_POS);

    if (data & (1 << 1))
        PORT(LCD_D1_REG) |= (1 << LCD_D1_POS);
    else
        PORT(LCD_D1_REG) &= ~(1 << LCD_D1_POS);

    if (data & (1 << 2))
        PORT(LCD_D2_REG) |= (1 << LCD_D2_POS);
    else
        PORT(LCD_D2_REG) &= ~(1 << LCD_D2_POS);

    if (data & (1 << 3))
        PORT(LCD_D3_REG) |= (1 << LCD_D3_POS);
    else
        PORT(LCD_D3_REG) &= ~(1 << LCD_D3_POS);

    if (data & (1 << 4))
        PORT(LCD_D4_REG) |= (1 << LCD_D4_POS);
    else
        PORT(LCD_D4_REG) &= ~(1 << LCD_D4_POS);

    if (data & (1 << 5))
        PORT(LCD_D5_REG) |= (1 << LCD_D5_POS);
    else
        PORT(LCD_D5_REG) &= ~(1 << LCD_D5_POS);

    if (data & (1 << 6))
        PORT(LCD_D6_REG) |= (1 << LCD_D6_POS);
    else
        PORT(LCD_D6_REG) &= ~(1 << LCD_D6_POS);

    if (data & (1 << 7))
        PORT(LCD_D7_REG) |= (1 << LCD_D7_POS);
    else
        PORT(LCD_D7_REG) &= ~(1 << LCD_D7_POS);
#else
    if (data & (1 << 0))
        PORT(LCD_D4_REG) |= (1 << LCD_D4_POS);
    else
        PORT(LCD_D4_REG) &= ~(1 << LCD_D4_POS);

    if (data & (1 << 1))
        PORT(LCD_D5_REG) |= (1 << LCD_D5_POS);
    else
        PORT(LCD_D5_REG) &= ~(1 << LCD_D5_POS);

    if (data & (1 << 2))
        PORT(LCD_D6_REG) |= (1 << LCD_D6_POS);
    else
        PORT(LCD_D6_REG) &= ~(1 << LCD_D6_POS);

    if (data & (1 << 3))
        PORT(LCD_D7_REG) |= (1 << LCD_D7_POS);
    else
        PORT(LCD_D7_REG) &= ~(1 << LCD_D7_POS);
#endif
}

/*
 * Pulses the EN pin (toggles on and off).
 */
static inline void _lcd_enable_pulse(void) {
    PORT(LCD_EN_REG) |= (1 << LCD_EN_POS);
    _delay_us(1);
    PORT(LCD_EN_REG) &= ~(1 << LCD_EN_POS);
}

/*
 * Reads a byte from the display.
 */
#if LCD_USE_BUSY_FLAG == 1
uint8_t _lcd_read(void) {
    uint8_t res = 0;

    _lcd_d_input();
    PORT(LCD_RW_REG) |= (1 << LCD_RW_POS);

#if LCD_USE_8BIT_MODE == 1
    PORT(LCD_EN_REG) |= (1 << LCD_EN_POS);
    _delay_us(1);
    res = _lcd_get_data_pins();
    PORT(LCD_EN_REG) &= ~(1 << LCD_EN_POS);
#else
    PORT(LCD_EN_REG) |= (1 << LCD_EN_POS);
    _delay_us(1);
    res = (_lcd_get_data_pins() << 4);
    PORT(LCD_EN_REG) &= ~(1 << LCD_EN_POS);

    PORT(LCD_EN_REG) |= (1 << LCD_EN_POS);
    _delay_us(1);
    res |= _lcd_get_data_pins();
    PORT(LCD_EN_REG) &= ~(1 << LCD_EN_POS);
#endif

    return res;
}
#endif

/*
 * Writes a byte to the display.
 */
void _lcd_write(uint8_t data) {
#if LCD_USE_BUSY_FLAG == 1
    PORT(LCD_RW_REG) &= ~(1 << LCD_RW_POS);
#endif
    _lcd_d_output();

#if LCD_USE_8BIT_MODE == 1
    _lcd_set_data_pins(data);
    _lcd_enable_pulse();
#else
    _lcd_set_data_pins(data >> 4);
    _lcd_enable_pulse();
    _lcd_set_data_pins(data);
    _lcd_enable_pulse();
#endif
}

/*
 * Reads the busy flag of the display.
 */
#if LCD_USE_BUSY_FLAG == 1
uint8_t _lcd_read_busy_flag(void) {
    PORT(LCD_RS_REG) &= ~(1 << LCD_RS_POS);
    return _lcd_read() & (1 << 7);
}
#endif

/*
 * Reads the address counter of the display.
 */
#if LCD_USE_BUSY_FLAG == 1
uint8_t _lcd_read_address_counter(void) {
    PORT(LCD_RS_REG) &= ~(1 << LCD_RS_POS);
    return _lcd_read() & ~(1 << 7);
}
#endif

/*
 * Sends a command to the display (RS = 0).
 */
void _lcd_send_command(uint8_t cmd) {
    PORT(LCD_RS_REG) &= ~(1 << LCD_RS_POS);
    _lcd_write(cmd);
#if LCD_USE_BUSY_FLAG == 1
    while (_lcd_read_busy_flag()) {}
#else
    _delay_ms(1.52); // (see: Execution Time (max), Table 6, datasheet pg. 24)
#endif
}

/*
 * Sends data to the display (RS = 1).
 */
void _lcd_send_data(uint8_t data) {
    PORT(LCD_RS_REG) |= (1 << LCD_RS_POS);
    _lcd_write(data);
#if LCD_USE_BUSY_FLAG == 1
    while (_lcd_read_busy_flag()) {}
#else
    _delay_us(41); // (see: Execution Time (max), Table 6, datasheet pg. 25)
#endif
}

/*
 * Initializes the display.
 */
void lcd_init(void) {
    // Setting RS, RW, EN and D0-D7 (or D4-D7) pins as outputs. 
    DDR(LCD_RS_REG) |= (1 << LCD_RS_POS);
#if LCD_USE_BUSY_FLAG == 1
    DDR(LCD_RW_REG) |= (1 << LCD_RW_POS);
#endif
    DDR(LCD_EN_REG) |= (1 << LCD_EN_POS);
    _lcd_d_output();

#if LCD_USE_8BIT_MODE == 1
    // Initialization by instructions (see: datasheet pg. 45).
    _delay_ms(15);
    _lcd_set_data_pins(0x30);
    _lcd_enable_pulse();
    _delay_ms(4.1);
    _lcd_set_data_pins(0x30);
    _lcd_enable_pulse();
    _delay_us(100);
    _lcd_set_data_pins(0x30);
    _lcd_enable_pulse();
    _delay_us(37);

#if LCD_USE_BUSY_FLAG == 1
    while (_lcd_read_busy_flag()) {}
#endif

    // 8-Bit Operation, 2-Line Display, 5x8 Characters
    _lcd_send_command(LCD_FUNCTION_SET |
        LCD_8_BITS | LCD_2_LINES | LCD_5x8_DOTS);
    _lcd_send_command(LCD_DISPLAY_ON_OFF_CONTROL |
        LCD_DISPLAY_ON | LCD_CURSOR_OFF | LCD_BLINK_OFF);
    _lcd_send_command(LCD_ENTRY_MODE_SET |
        LCD_INCREMENT);
    _lcd_send_command(LCD_CLEAR_DISPLAY);
#else
    // Initialization by instructions (see: datasheet pg. 46).
    _delay_ms(15);
    _lcd_set_data_pins(0x3);
    _lcd_enable_pulse();
    _delay_ms(4.1);
    _lcd_set_data_pins(0x3);
    _lcd_enable_pulse();
    _delay_us(100);
    _lcd_set_data_pins(0x3);
    _lcd_enable_pulse();
    _delay_us(37);

    // Setting to 4-bit operation.
    _lcd_set_data_pins(0x2);
    _lcd_enable_pulse();
    _delay_us(37);

#if LCD_USE_BUSY_FLAG == 1
    while (_lcd_read_busy_flag()) {}
#endif

    // 4-Bit Operation, 2-Line Display, 5x8 Characters
    _lcd_send_command(LCD_FUNCTION_SET |
        LCD_4_BITS | LCD_2_LINES | LCD_5x8_DOTS);
    _lcd_send_command(LCD_DISPLAY_ON_OFF_CONTROL |
        LCD_DISPLAY_ON | LCD_CURSOR_OFF | LCD_BLINK_OFF);
    _lcd_send_command(LCD_ENTRY_MODE_SET |
        LCD_INCREMENT);
    _lcd_send_command(LCD_CLEAR_DISPLAY);
#endif
}

/*
 * Clears the display contents and sets the cursor position to (0, 0).
 */
void lcd_clear(void) {
    _lcd_send_command(LCD_CLEAR_DISPLAY);
}

/*
 * Sets the cursor position to (0, 0).
 */
void lcd_home(void) {
    _lcd_send_command(LCD_RETURN_HOME);
}

/*
 * Sets the cursor position on the display.
 */
void lcd_set_pos(uint8_t x, uint8_t y) {
    switch (y) {
        case 0: y = LCD_LINE0; break;

#if LCD_ROWS > 1
        case 1: y = LCD_LINE1; break;
#endif

#if LCD_ROWS > 2
        case 2: y = LCD_LINE2; break;
#endif

#if LCD_ROWS > 3
        case 3: y = LCD_LINE3; break;
#endif

        default: return;
    }

    _lcd_send_command(LCD_SET_DDRAM_ADDRESS + y + x);
}

/*
 * Prints a single character on the display.
 */
void lcd_putc(char c) {
    _lcd_send_data(c);
}

/*
 * Prints a string of characters on the display.
 */
void lcd_print_str(char *s) {
    char c;
    while ((c = *s++)) {
        _lcd_send_data(c);
    }
}

/*
 * Prints a decimal number on the display.
 */
void lcd_print_dec(int32_t val) {
    char buf[LCD_COLS+1];
    lcd_print_str(ltoa(val, buf, 10));
}

/*
 * Prints a hexadecimal number on the display.
 */
void lcd_print_hex(uint32_t val) {
    char buf[LCD_COLS+1];
    lcd_print_str(ltoa(val, buf, 16));
}

/*
 * Defines a custom character in CGRAM memory.
 * For 5x8 characters mode, there are 8 CGRAM locations available.
 * The defined characters can then be printed on the display
 * using 0..7 as an argument of lcd_putc(char).
 *
 * `idx` is the index of a custom character in CGRAM memory.
 * `cmap` must point on an array of at least 8 elements.
 */
void lcd_define_char(uint8_t idx, const uint8_t *cmap) {
    uint8_t i;
#if LCD_USE_BUSY_FLAG == 1
    uint8_t ac;
    ac = _lcd_read_address_counter();
#endif

    _lcd_send_command(LCD_SET_CGRAM_ADDRESS |
        ((idx & 0x7) << 3));

    for (i = 0; i < 8; ++i) {
        _lcd_send_data(cmap[i]);
    }

#if LCD_USE_BUSY_FLAG == 1
    _lcd_send_command(LCD_SET_DDRAM_ADDRESS |
        ac);
#endif
}
