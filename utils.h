#ifndef UTILS_H_
#define UTILS_H_

#define _CONCAT(a,b) a##b
#define DDR(x)  _CONCAT(DDR,x)
#define PIN(x)  _CONCAT(PIN,x)
#define PORT(x) _CONCAT(PORT,x)

#endif /* UTILS_H_ */
