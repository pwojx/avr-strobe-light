#ifndef ENCODER_H_
#define ENCODER_H_

#include <stdint.h>

/******** Encoder config ********/
#define ENC_USE_BUILTIN_PULLUPS  0
#define ENC_HALF_STEP_ENCODER    0

#define ENC_A_REG    D
#define ENC_A_POS    2

#define ENC_B_REG    D
#define ENC_B_POS    3
/********************************/

/******** Encoder steps *********/
#define ENC_DIR_NONE 0x0     // No complete step yet.
#define ENC_DIR_CW   0x10    // Clockwise step.
#define ENC_DIR_CCW  0x20    // Anti-clockwise step.
/********************************/

/****** Encoder functions *******/
void encoder_init(void);
uint8_t encoder_process(void);
/********************************/

#endif /* ENCODER_H_ */
