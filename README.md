# AVR Strobe Light

## Table of contents
[[_TOC_]]

## About this project
This is a project of a tachometer (an instrument that measures the rotation speed of an object, such as a motor shaft or a fan) whose principle of operation is based on the [stroboscopic effect](https://en.wikipedia.org/wiki/Stroboscopic_effect). The device allows you to measure rotation speeds from 60 to 300000 RPM which corresponds to the frequency of flashes ranging from 1 to 5000 Hz. Moreover, the device can also act as a simple (and quite powerful) flashlight. Be aware that the strobe lights may trigger epileptic seizures to people who are photosensitive!

## How to use it
To measure the rotation speed of a specific object, it is required to point the device at the object, turn the light on and slowly decrease the frequency of the flashes, down to a point when the observed image is stationary and not multiplied. It is helpful to look for reference points that are easily distinguishable, such as indentations or inscriptions. On a "multiplied" image such reference points occur more times than in reality (when the object is not rotating). That means that the tuning process is not completed and must be continued. If the object does not have such properties (reference points), it is advised to mark a sign with a pen or glue a piece of paper on it to easily determine whether the adjusted image is right (of course this needs to be done before the object starts to rotate). Then it is recommended to check whether the measurement is correct by doubling the current rate of flashes to check if it is the harmonic frequency or not. If the image has broken after this process, this means that the previous frequency was actually correct. After obtaining the correct image, the measured value can be read from the device screen. The process of collecting rotation speed measurements using a stroboscope is explained in more details [here](https://www.youtube.com/watch?v=KPrSPqfVJhA).

## Electrical schematic
The electrical schematic of the device is presented below.

![](img/schematic_avr_strobe_light.png "Electrical schematic")

## PCB
The PCB layout has been designed based on the schematic and the PCB has been made using a single-sided copper laminate and the toner transfer method.

![](img/pcb_avr_strobe_light.png "PCB design")

All components have been mounted on the PCB, starting from the MCU, which is the only SMD component.

![](img/pcb.jpg "PCB with mounted components")

## Enclosure
A suitable enclosure has been designed using 3D modeling software.

![](img/ASL.png "Enclosure 3D model")

## The final result
The PCB has been placed inside the enclosure along with a battery pack consisting of three 18650 Li-ion cells. The LED lamp has been screwed to the upper side of the enclosure and the switches, encoder and LCD have been mounted on the front panel. All elements were connected using wires.

****TODO: add the photo****

![](img/avr_strobe_light.png "AVR Strobe Light")

## User interface
The main user interface is shown in the figure below. Current RPM and duty cycle values are displayed in the middle of the LCD. On the left side there is an arrow that points at the value that can be changed at the moment by turning the encoder. The arrow position can be changed using the "RPM/%" toggle switch (SW3 on the schematic). In the upper right corner there is an adjustment mode indicator that changes shape depending on the currently active mode (fine/coarse). In fine mode, values change by 1 step and in coarse mode, by 10 steps. Below, in the bottom right corner there is an indicator that shows whether the light is turned on or not.

When the duty cycle is set to the minimum value at a specific RPM, it can be interpreted as the frequency of light flashes. For example if the value is 10.00%, the frequency is 1000 Hz and if it's 0.83%, the frequency is 83 Hz - the decimal point is omitted. Remember that this is only the case for the lowest duty cycle!

![](img/interface.jpg "User interface")

The device can also be used as a flashlight by turning the RPM all the way up beyond the maximum value. In this mode, the LEDs are turned on completely and are not flashing. The current battery voltage measured by the ADC is also shown on the display.

![](img/flashlight_screen.jpg "Flashlight screen")

## Icons

Fine adjustment icon                         | Coarse adjustment icon                           | Light OFF icon                          | Light ON icon
:--------------------------------------------|--------------------------------------------------|-----------------------------------------|-------------------------------------:
![](img/fine_adj_icon.png "Fine adjustment") | ![](img/coarse_adj_icon.png "Coarse adjustment") | ![](img/light_off_icon.png "Light OFF") | ![](img/light_on_icon.png "Light ON")

## Low battery indicator and overdischarge protection
****This will be described in the near future.****