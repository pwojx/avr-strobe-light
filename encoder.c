/*
 * Copyright (C) 2011 Ben Buxton <bb@cactii.net>
 * Copyright (C) 2021 Paweł Wojaczek <pwojx@protonmail.com>
 *
 * The original library, written for Arduino by Ben Buxton is available here:
 * https://github.com/buxtronix/arduino/tree/master/libraries/Rotary
 *
 * This version of the library has been rewritten in C language to provide
 * support for programming of AVR microcontrollers using avr-gcc.
 * For more information on how the algorithm works, see the link above.
 */

#include "encoder.h"

#include <avr/io.h>
#include <avr/pgmspace.h>

#include "utils.h"

/*
 * State tables definitions.
 */
#define R_START        0x0

#if ENC_HALF_STEP_ENCODER == 1
#define R_CCW_BEGIN    0x1
#define R_CW_BEGIN     0x2
#define R_START_M      0x3
#define R_CW_BEGIN_M   0x4
#define R_CCW_BEGIN_M  0x5

// Half-step table
const uint8_t ttable[6][4] PROGMEM = {
    // R_START (00)
    {R_START_M,               R_CW_BEGIN,     R_CCW_BEGIN,  R_START},
    // R_CCW_BEGIN
    {R_START_M | ENC_DIR_CCW, R_START,        R_CCW_BEGIN,  R_START},
    // R_CW_BEGIN
    {R_START_M | ENC_DIR_CW,  R_CW_BEGIN,     R_START,      R_START},
    // R_START_M (11)
    {R_START_M,               R_CCW_BEGIN_M,  R_CW_BEGIN_M, R_START},
    // R_CW_BEGIN_M
    {R_START_M,               R_START_M,      R_CW_BEGIN_M, R_START | ENC_DIR_CW},
    // R_CCW_BEGIN_M
    {R_START_M,               R_CCW_BEGIN_M,  R_START_M,    R_START | ENC_DIR_CCW},
};
#else
#define R_CW_FINAL     0x1
#define R_CW_BEGIN     0x2
#define R_CW_NEXT      0x3
#define R_CCW_BEGIN    0x4
#define R_CCW_FINAL    0x5
#define R_CCW_NEXT     0x6

// Full-step table
const uint8_t ttable[7][4] PROGMEM = {
    // R_START
    {R_START,    R_CW_BEGIN,  R_CCW_BEGIN, R_START},
    // R_CW_FINAL
    {R_CW_NEXT,  R_START,     R_CW_FINAL,  R_START | ENC_DIR_CW},
    // R_CW_BEGIN
    {R_CW_NEXT,  R_CW_BEGIN,  R_START,     R_START},
    // R_CW_NEXT
    {R_CW_NEXT,  R_CW_BEGIN,  R_CW_FINAL,  R_START},
    // R_CCW_BEGIN
    {R_CCW_NEXT, R_START,     R_CCW_BEGIN, R_START},
    // R_CCW_FINAL
    {R_CCW_NEXT, R_CCW_FINAL, R_START,     R_START | ENC_DIR_CCW},
    // R_CCW_NEXT
    {R_CCW_NEXT, R_CCW_FINAL, R_CCW_BEGIN, R_START},
};
#endif

/*
 * Initializes the encoder.
 */
void encoder_init(void) {
    DDR(ENC_A_REG) &= ~(1 << ENC_A_POS);
    DDR(ENC_B_REG) &= ~(1 << ENC_B_POS);
#if ENC_USE_BUILTIN_PULLUPS == 1
    PORT(ENC_A_REG) |= (1 << ENC_A_POS);
    PORT(ENC_B_REG) |= (1 << ENC_B_POS);
#endif
}

/*
 * Processes the output of the encoder.
 */
uint8_t encoder_process(void) {
    static uint8_t state = R_START;

    // Grab state of input pins.
    uint8_t pinstate = 0;
    if (PIN(ENC_B_REG) & (1 << ENC_B_POS)) pinstate |= 0b10;
    if (PIN(ENC_A_REG) & (1 << ENC_A_POS)) pinstate |= 0b01;

    // Determine new state from the pins and state table.
    state = pgm_read_byte(&ttable[state & 0xf][pinstate]);

    // Return emit bits, ie the generated event.
    return state & 0x30;
}
